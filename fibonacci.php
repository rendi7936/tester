<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Fibonnaci</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
</head>

<body>
  <nav>
    <div class="nav-wrapper indigo lighten-1">
      <a href="hello.php" class="brand-logo">PHP</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="hello.php">Hello</a></li>
        <li><a href="fibonacci.php">Fibonacci</a></li>
        <li><a href="database.php">SelectData</a></li>
      </ul>
    </div>
  </nav>
  <br>
  <form method="POST" action="">
    <div class="row">
      <div class="input-field col s3">
        <input name="Number" type="text" class="validate">
        <label for="Number">Number</label>
        <input class="waves-effect waves-light btn" type="submit">
      </div>
    </div>
  </form>

  <?php
    $Number=$_POST["Number"];

    function cari_fibonacci($urutan)
    {
  // siapkan 2 angka awalS
      $angka_sebelumnya=0;
      $angka_sekarang=1;

      for ($i=0; $i<$urutan-1; $i++)
      {
    // hitung angka fibonacci
        $output = $angka_sekarang + $angka_sebelumnya;

    //siapkan angka untuk perhitungan berikutnya
        $angka_sebelumnya = $angka_sekarang;
        $angka_sekarang = $output;
      }
      return $output;
    }

    echo cari_fibonacci($Number);

  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
</body>

</html>
