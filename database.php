<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>SelectData</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
</head>

<body>
  <nav>
    <div class="nav-wrapper indigo lighten-1">
      <a href="hello.php" class="brand-logo">PHP</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="hello.php">Hello</a></li>
        <li><a href="fibonacci.php">Fibonacci</a></li>
        <li><a href="database.php">SelectData</a></li>
      </ul>
    </div>
  </nav>
        <?php
        /* Attempt MySQL server connection. Assuming you are running MySQL
        server with default setting (user 'root' with no password) */
        $link = mysqli_connect("localhost", "root", "root", "CSV_DB");
         
        // Check connection
        if($link === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }
         
        // Attempt select query execution
        $sql = "SELECT * FROM TBL_NAME LIMIT 2000";
        if($result = mysqli_query($link, $sql)){
            if(mysqli_num_rows($result) > 0){
                echo "<table class='container'>";
                  echo "<thead>";
                    echo "<tr>";
                        echo "<th>BEGIN_TIME</th>";
                        echo "<th>END_TIME</th>";
                        echo "<th>EVENT_ID</th>";
                        echo "<th>STATE</th>";
                    echo "</tr>";
                  echo "</thead>";
                while($row = mysqli_fetch_array($result)){
                  echo "<tbody>";
                    echo "<tr>";
                        echo "<td>" . $row['BEGIN_TIME'] . "</td>";
                        echo "<td>" . $row['END_TIME'] . "</td>";
                        echo "<td>" . $row['EVENT_ID'] . "</td>";
                        echo "<td>" . $row['STATE'] . "</td>";
                    echo "</tr>";
                  echo "</tbody>";
                }
                echo "</table>";
                // Free result set
                mysqli_free_result($result);
            } else{
                echo "No records matching your query were found.";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
        }
         
        // Close connection
        mysqli_close($link);
        ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
</body>

</html>
