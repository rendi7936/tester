<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>HelloWorld</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
</head>

<body>
  <nav>
    <div class="nav-wrapper indigo lighten-1">
      <a href="hello.php" class="brand-logo">PHP</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="hello.php">Hello</a></li>
        <li><a href="fibonacci.php">Fibonacci</a></li>
        <li><a href="database.php">SelectData</a></li>
      </ul>
    </div>
  </nav>
  <h1></h1>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
</body>

</html>
